<html>
    <head>
        <title>Test</title>
    </head>
    <body>
        <div class="col-lg-12">
            <div class="well">
                <div class="page-header">
                    <h1>Understanding Views and Controllers</h1>
                </div>
            </div>
            <div class="btn btn-lg btn-primary"><?php echo $message ?></div>
            <h2>Please fill this form and submit...</h2>
            <form method="post" action="" class="form-group">
                <label>Username:</label><input name="username" type="text">
                <label>Email:</label><input name="email" type="email">
                <button type="submit">Submit</button>
            </form>
            
        </div>
    </body>
</html>

