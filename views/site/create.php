<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */

$this->title = 'Guest Book';
?>
<div class="site-index">
    
    <h1>ADD GUEST</h1>

    <div class="body-content">
        <?php $form = ActiveForm::begin() ?>
        <div class="row">
            <div class="form-group">
                <div class="col-lg-6">
                    <?= $form->field($guest, 'name'); ?>
                </div>
            </div>
        </div>
        
         <div class="row">
            <div class="form-group">
                <div class="col-lg-6">
                    <?= $form->field($guest, 'phone_number'); ?>
                </div>
            </div>
        </div>
        
         <div class="row">
            <div class="form-group">
                <div class="col-lg-6">
                    <?= $form->field($guest, 'address')->textarea(['rows'=> '6']) ?>
                </div>
            </div>
        </div>
        
         <div class="row">
            <div class="form-group">
                <div class="col-lg-6">
                    <?php $items=['Approved'=>'Approved', 'Unpproved'=>'Unapproved', 'Pending'=>'Pending']; ?>
                    <?= $form->field($guest, 'status')->dropDownList($items, ['prompt'=> 'Select']) ?>
                </div>
            </div>
        </div>
        
         <div class="row">
            <div class="form-group">
                <div class="col-lg-6">
                    <div class="col-lg-3">
                        <?= Html::submitButton('Add Guest', ['class'=> 'btn btn-primary']); ?>
                    </div>
                    <div class="col-lg-2">
<!--                        Back tick to send user back to the home url-->
                        <a href="<?php echo yii::$app->homeUrl; ?>" class="btn btn-primary">Go Back</a>
                    </div>
                </div>
                </div>
            </div>
        </div>
        <?php ActiveForm::end() ?>
    </div>
</div>
 
