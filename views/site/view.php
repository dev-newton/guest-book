<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */

$this->title = 'Guest Book';
?>
<div class="site-index">
    
    <h1>VIEW GUEST</h1>

    <div class="body-content">
       <ul class="list-group">
        <li class="list-group-item d-flex justify-content-between align-items-center">
          <?php echo $guest->name ?>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center">
          <?php echo $guest->phone_number ?>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center">
          <?php echo $guest->address ?>
        </li>
         <li class="list-group-item d-flex justify-content-between align-items-center">
          <?php echo $guest->status ?>
        </li>
      </ul>
        <div class="row">
           <a href="<?php echo yii::$app->homeUrl; ?>" class="btn btn-primary">Go Back</a>
        </div>
    </div>
</div>
 
