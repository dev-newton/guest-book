<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <?php if(yii::$app->session->hasFlash('message')): ?>
            <div class="alert alert-dismissible alert-success">
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <?php echo yii::$app->session->getFlash('message') ?>
        </div>
    <?php endif; ?>
    <div class="jumbotron">
        <h1 style="color: #337ab7">Guest Book Application</h1>

        
    </div>
    <div class="row">
        <span style="margin-bottom: 20px;"><?= Html::a('Add Guest', ['/site/create'],['class'=>'btn btn-primary']) ?></span>
    </div>
    <div class="body-content">

        <div class="row">
        <table class="table table-hover">
          <thead>
            <tr>
              <th scope="col">ID</th>
              <th scope="col">Name</th>
              <th scope="col">Phone Number</th>
              <th scope="col">Address</th>
              <th scope="col">Status</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>
              <?php if(count($guests) > 0): ?>
                <?php foreach ($guests as $guest): ?>
            <tr class="table-active">
              <th scope="row"><?php echo $guest->id; ?></th>
              <td><?php echo $guest->name; ?></td>
              <td><?php echo $guest->phone_number; ?></td>
              <td><?php echo $guest->address; ?></td>          
              <td><?php echo $guest->status; ?></td>
              <td>
                  <span><?= Html::a('View', ['view', 'id'=> $guest->id], ['class'=> 'label label-primary']) ?></span>
                  <span><?= Html::a('Update',['update', 'id'=> $guest->id], ['class'=> 'label label-default']) ?></span>
                  <span><?= Html::a('Delete',['delete', 'id'=> $guest->id], ['class'=> 'label label-danger']) ?></span>
                  
              </td>
            </tr>
                <?php endforeach; ?>
           <?php else: ?>
            <tr>
                <td>No Records Found</td>
            </tr>
            <?php endif; ?>
          </tbody>
        </table>
        </div>

    </div>
</div>
 