<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Guests;
use yii\web\NotFoundHttpException;

class SiteController extends Controller
{
//    public $layout = "newlayout";
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['about', 'contact'],
                'rules' => [
                    [
                        'actions' => ['about'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['contact', 'about'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    //select frm db
    public function actionIndex()
    {
        $guests = Guests::find()->all();
//        echo '<pre>';
//        print_r($guests);
//        echo '</pre>';
//        exit();
        return $this->render('home', ['guests' => $guests]);
    }

    //insert to db
    public function actionCreate() {
        $guest = new Guests();
        $formData = yii::$app->request->post();
        if($guest->load($formData)){
            if($guest->save()){
                yii::$app->getSession()->setFlash('message','New Guest has been added Successfully');
                return $this->redirect(['index']);
                
            }
            else {
                 yii::$app->getSession()->setFlash('message','Failed to add New Guest');
            }
        }
        return $this->render('create', ['guest' => $guest]);
    }
    
    public function actionView($id) {
        $guest = Guests::findOne($id);
        return $this->render('view', ['guest'=>$guest]);
    }

    public function actionUpdate($id) {
        $guest = Guests::findOne($id);
        if($guest->load(yii::$app->request->post()) && $guest->save())  {
            yii::$app->getSession()->setFlash('message', 'Guest Profile Updated Successfully');
            return $this->redirect(['index', 'id'=>$guest->id]);      
        }
        else {
            return $this->render('update', ['guest'=> $guest]);
        }

    }
    
    public function actionDelete($id) {
        $guest = Guests::findOne($id)->delete();
        if($guest) {
            yii::$app->getSession()->setFlash('message', 'Guest Profile has been Deleted Successfully');
            return $this->redirect(['index', 'id'=>$guest->id]);      
        }
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
//        \Yii::$app->view->on(View::EVENT_BEGIN_BODY, function() {
//            echo date('m.d.Y H:i:s');
//        });
        $email = "admin@support.com";
        $phone = "+2349057387046";
        
        return $this->render('about', [
            'email' => $email,
            'phone' => $phone
        ]);
    }
    
    public function actionShowContactModel() {
        $mContactForm = new \app\models\ContactForm();
        $mContactForm->name = "contactForm";
        $mContactForm->email = "user@gmail.com";
        $mContactForm->subject = "subject";
        $mContactForm->body = "body";
        
        return \yii\helpers\Json::encode($mContactForm->attributes);
    }
    
    public function actionTestWidget() {
        return $this->render('testwidget');
    }
    
    public function actionTestGet() {
        
        
      $req = Yii::$app->request;
      //to get header information
      var_dump($req->userHost);
      var_dump($req->userIP);
      
   //the URL without the host
//   var_dump(Yii::$app->request->url);
//   
//   //the whole URL including the host path
//   var_dump(Yii::$app->request->absoluteUrl);     
//        //the host of the URL
//   var_dump(Yii::$app->request->hostInfo);
//   
//   //the part after the entry script and before the question mark
//   var_dump(Yii::$app->request->pathInfo);
//   
//   //the part after the question mark
//   var_dump(Yii::$app->request->queryString);
//   
//   //the part after the host and before the entry script
//   var_dump(Yii::$app->request->baseUrl);
//   
//   //the URL without path info and query string
//   var_dump(Yii::$app->request->scriptUrl);
//   
//   //the host name in the URL
//   var_dump(Yii::$app->request->serverName);
//   
//   //the port used by the web server
//   var_dump(Yii::$app->request->serverPort);
        
//        if ($req->isAjax) {
//          echo "the request is AJAX";
//        }
//        if ($req->isGet) {
//           echo "the request is GET";
//        }
//        if ($req->isPost) {
//           echo "the request is POST";
//        }
//        if ($req->isPut) {
//           echo "the request is PUT";
//        }
     }
     
     public function actionTestResponse() {
        // \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
//         Yii::$app->response->statusCode = 201;
//         throw new \yii\web\GoneHttpException;
//           return [
//               'id' => '1',
//                'name' => 'Ivan',
//                'age' => 24,
//                'country' => 'Poland',
//                'city' => 'Warsaw'
//           ];
         
         \Yii::$app->response->sendFile('favicon.ico');
     }
     
     public function actionMaintenance() {
         echo "<h1>Mintenance is going on</h1>";
     }
     
     public function actionRoutes() {
         return $this->render('routes');
     }
     
     public function actionRegistration() {
         $model = new \app\models\RegistrationForm;
         
          if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->refresh();
        }
        
         return $this->render('registration', ['model' => $model]);
     }
     
     public function actionShowError() {
//         try {
//             5/0;
//         } catch (ErrorException $e) {
//             Yii::warning("bad division by zero");
//         }
//         
         //execution continues
         throw new NotFoundHttpException("Something unexpected happened");
     }
     
     public function actionAuth() {
         $password = "asd%#G3";
         
        //generates password hash
        $hash = Yii::$app->getSecurity()->generatePasswordHash($password);
        var_dump($hash);

        //validates password hash
        if (Yii::$app->getSecurity()->validatePassword($password, $hash)) {
           echo "correct password";
        } else {
           echo "incorrect password";
        }
        
        //generate a token
   $key = Yii::$app->getSecurity()->generateRandomString();
   var_dump($key);
   
   //encrypt data with a secret key
   $encryptedData = Yii::$app->getSecurity()->encryptByPassword("mydata", $key);
   var_dump($encryptedData);
   
   //decrypt data with a secret key
   $data = Yii::$app->getSecurity()->decryptByPassword($encryptedData, $key);
   var_dump($data);
   
   //hash data with a secret key
   $data = Yii::$app->getSecurity()->hashData("mygenuinedata", $key);
   var_dump($data);
   
   //validate data with a secret key
   $data = Yii::$app->getSecurity()->validateData($data, $key);
   var_dump($data);
     }
}
